from rest_framework import viewsets
from .models import Language, Paradigm, Programmer
from .serializer import LanguageSerializer
from .serializer import ParadigmSerializer, ProgrammerSerializer


class LanguageView(viewsets.ModelViewSet):
    """ Viewset for model Language and LanguageSerializer
    """
    queryset = Language.objects.all()
    serializer_class = LanguageSerializer


class ParadigmView(viewsets.ModelViewSet):
    """ Viewset for model Paradigm and ParadigmSerializer
    """
    queryset = Paradigm.objects.all()
    serializer_class = ParadigmSerializer


class ProgrammerView(viewsets.ModelViewSet):
    """ Viewset for model Programmer and ProgrammerSerializer
    """
    queryset = Programmer.objects.all()
    serializer_class = ProgrammerSerializer
