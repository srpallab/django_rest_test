from rest_framework import serializers
from .models import Language, Paradigm, Programmer


class LanguageSerializer(serializers.HyperlinkedModelSerializer):
    """ Serializer for the Language model
    """
    class Meta:
        model = Language
        fields = ('id', 'url', 'name', 'paradigm')


class ParadigmSerializer(serializers.HyperlinkedModelSerializer):
    """ Serializer for the Paradigm model

    """
    class Meta:
        model = Paradigm
        fields = ('id', 'url', 'name')


class ProgrammerSerializer(serializers.HyperlinkedModelSerializer):
    """ Serializer for the Programmer model

    """
    class Meta:
        model = Programmer
        fields = ('id', 'url', 'name', 'languages')
